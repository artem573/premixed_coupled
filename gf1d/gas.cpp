#include <iostream>
#include "../gasPhase1D.h"
#include "../reader.h"

int main()
{
  Reader data("input_data");
  GasPhase1D fld;

  fld.restoreSolution(data.getString("initial_g"));
  
  double T_f0 = data.getDouble("T_f0");
  double m_dot_f0 = data.getDouble("m_dot_f0");
  auto [qw_mol, qw_rad] = fld.solve(T_f0, m_dot_f0);
  std::cout << "qw_mol: " << qw_mol << ", qw_rad: " << qw_rad << ", mdot: " << fld.mdot() << "\n";

  fld.saveSolutionXML("solution_gas.xml");
  fld.saveSolutionCSV("solution_gas.csv");

  return 0;
}
