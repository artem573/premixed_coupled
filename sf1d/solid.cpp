#include <iostream>
#include "../solidPhase1D.h"
#include "../reader.h"

int main()
{
  Reader data("input_data");
  double qw0 = data.getDouble("qw0");
  SolidPhase1D sld;

  sld.restoreSolution("solution_solid.dat");

  auto [m_dot, Tw] = sld.solve(qw0);
  std::cout << "YO: " << m_dot << " " << Tw << "\n";

  sld.saveSolution("solution_solid.dat");
  
  return 0;
}
