#ifndef _SOLIDPHASE1D_H_
#define _SOLIDPHASE1D_H_

#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <sstream>
#include <string.h>
#include <math.h>
#include <utility>
#include "reader.h"

#include "cantera/IdealGasMix.h"
#include "cantera/transport.h"


class SolidPhase1D : public Reader
{

private:

  const double PInf, ZInf;
  const double Lx_s;
  const int Nx_s;
  int N_it;
  const double Ts0;
  const double R0;
  const double relax_T;
  const double relax_alpha;
  const double dx;
  double dt;
  double tol_abs, tol_rel;
  const double rho_s, C_s, Q_s, k_s, E_s, ordr_s, ordr_m, alpha_star;
  const double lam_s, lam_melt;
  const double TmC, C_L0, C_L1, C_R0, C_R1;
  double m_dot, m_dot0;
  double Tw;

  std::vector<double> T, T0, W, alpha, x;
  std::vector<double> rho, C, lam, lam_w, lam_e;
  std::vector<double> a, b, c, d;

  double sqr(double a) const;
  double calcLams(double Tv) const;
  double calcCs(double Tv) const;
  void setMesh();
  void setCoeffsT();
  void TDMA(std::vector<double> &var, double relax, double var_max, double var_min);
  void relaxCoeffs(std::vector<double> &var, double relax);
  void calcMdot();
  void updateThermo();
  void setCoeffsAlpha();
  double calcWs(double alphav, double Tv);
  void setLeftBCFixedValue(double val);
  void setRightBCFixedValue(double val);
  void setLeftBCGradient(double grad);
  void setRightBCGradient(double grad);
  bool checkConvergence();
  void checkDt(int &it_dt);

public:

  SolidPhase1D();
  
  std::pair<double, double> solve(double q_g=0);
  void saveSolution(const std::string fname);
  void restoreSolution(const std::string fname);
  void reset();
  
};

#endif // _SOLIDPHASE1D_H_
