#ifndef _READER_H_
#define _READER_H_

#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <unordered_map>
#include <string.h>

class Reader
{
  std::unordered_map<std::string, std::string> um;

 public:

  Reader(std::string file_name);
  void readConsts(std::string file_name);
  int getInt(std::string var_name) const;
  double getDouble(std::string var_name) const;
  std::string getString(std::string var_name) const;
  std::string getValue(std::string var_name) const;
};

#endif // _READER_H_
