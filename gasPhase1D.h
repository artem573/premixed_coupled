#ifndef _GASPHASE1D_H_
#define _GASPHASE1D_H_

#include "cantera/oneD/Sim1D.h"
#include "cantera/oneD/Inlet1D.h"
#include "cantera/oneD/StFlow.h"
#include "cantera/IdealGasMix.h"
#include "cantera/transport.h"

#include <fstream>
#include <iostream>
#include <vector>
#include <string.h>
#include <utility>

#include "reader.h"

class GasPhase1D : public Reader
{
  
private:
  
  Cantera::IdealGasMix gas_mix;

  Cantera::Inlet1D inlet;
  Cantera::Outlet1D outlet;
  Cantera::StFlow flow;
  std::unique_ptr<Cantera::Transport> trmix;
  std::unique_ptr<Cantera::Transport> trmulti;

  Cantera::Sim1D *flame;
  
  const double p0;
  const double width;

  const bool burner_flame;

  bool make_init_guess;

  void setupMesh();
  void makeInitialGuess(double m_dot);
  double mixFandOStreams(Cantera::IdealGasMix& gas_mix, double T_f, double m_dot_f);

public:

  GasPhase1D();

  std::pair<double, double> solve(double T_f, double m_dot_f);
  void saveSolutionXML(const std::string fname);
  void saveSolutionCSV(const std::string fname);
  void restoreSolution(const std::string fname);
  void setFixedT();
  void setExtQ();
  double mdot() { return inlet.mdot(); }
  void refineGridX2();
};
  

#endif // _GASPHASE1D_H_
