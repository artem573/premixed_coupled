#include "cantera/oneD/Sim1D.h"
#include "cantera/oneD/Inlet1D.h"
#include "cantera/oneD/StFlow.h"
#include "cantera/IdealGasMix.h"
#include "cantera/transport.h"

#include <fstream>
#include <iostream>
#include <vector>
#include <string.h>

#include "../reader.h"

int main()
{
  Reader data("input_data");
  const double p0 = data.getDouble("p0");
  const double T_f = data.getDouble("T_f0");
  const double T_o = data.getDouble("T_o");
  const double m_dot_f = data.getDouble("m_dot_f0");
  const double m_dot_o = data.getDouble("m_dot_o");
  const double m_dot_mix = m_dot_f + m_dot_o;
  const std::string mech = data.getString("mechanism");
  const std::string f_comp_y = data.getString("f_comp_y");
  const std::string o_comp_y = data.getString("o_comp_y");

  Cantera::IdealGasMix gas_f(mech);
  gas_f.setState_TPY(T_f, p0, f_comp_y);
  Cantera::IdealGasMix gas_o(mech);
  gas_o.setState_TPY(T_o, p0, o_comp_y);
  Cantera::IdealGasMix mixture(mech);

  size_t n_sp = mixture.nSpecies();

  Cantera::vector_fp y1(n_sp);
  gas_f.getMassFractions(&y1[0]);
  double cp1 = gas_f.cp_mass();
    
  Cantera::vector_fp y2(n_sp);
  gas_o.getMassFractions(&y2[0]);
  double cp2 = gas_o.cp_mass();
  
  Cantera::vector_fp y_mix(n_sp, 0);
  for (size_t i = 0; i < n_sp; ++i)
  {
    y_mix[i] = (y1[i]*m_dot_f + y2[i]*m_dot_o) / m_dot_mix;
  }
  double cp_mix = (cp1*m_dot_f + cp2*m_dot_o) / m_dot_mix;
  double T_mix = (cp1*T_f*m_dot_f + cp2*T_o*m_dot_o) / (m_dot_mix * cp_mix);

  // TODO: make getBool with values check
  if (data.getInt("set_mix_g"))
    {
      if (data.getString("mix_comp_type") == "x")
        {
          mixture.setState_TPX(data.getDouble("T_mix"), p0, data.getString("mix_comp"));
        }
      else if (data.getString("mix_comp_type") == "y")
        {
          mixture.setState_TPY(data.getDouble("T_mix"), p0, data.getString("mix_comp"));
        }
      else
        {
          throw Cantera::CanteraError("gasPhase1D::solve",
                                      "'mix_comp_type' in 'input_data' should be 'x' or 'y'");
        }
    }
  else
    {
      mixture.setState_TPY(T_mix, p0, &y_mix[0]);
    }


  
  std::cout << mixture.report() << "\n";
  mixture.equilibrate("HP");
  std::cout << mixture.report() << "\n";
  mixture.setState_TP(T_o, p0);
  std::cout << mixture.report() << "\n";
  // std::cout << "Tad: " << mixture.temperature() << "\n";
  
  return 0;
}
