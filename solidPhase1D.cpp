#include "solidPhase1D.h"

SolidPhase1D::SolidPhase1D():

  Reader("input_data"),
  PInf(getDouble("PInf")), ZInf(getDouble("ZInf")),
  Lx_s(getDouble("Lx_s")),
  Nx_s(getInt("Nx_s")),
  N_it(getInt("N_it_s")),
  Ts0(getDouble("Ts0")),
  R0(getDouble("R0")),
  relax_T(getDouble("relax_T")),
  relax_alpha(getDouble("relax_alpha")),
  dx(Lx_s/Nx_s),
  dt(getDouble("dt")),
  tol_abs(getDouble("tol_abs_s")), tol_rel(getDouble("tol_rel_s")),
  rho_s(getDouble("rho_s")), C_s(getDouble("C_s")), Q_s(getDouble("Q_s")),
  k_s(getDouble("k_s")),
  E_s(getDouble("E_s")), ordr_s(getDouble("ordr_s")),
  ordr_m(getDouble("ordr_m")), alpha_star(getDouble("alpha_star")),
  lam_s(getDouble("lam_s")), lam_melt(getDouble("lam_melt")),
  TmC(getDouble("TmC")), C_L0(getDouble("C_L0")), C_L1(getDouble("C_L1")), C_R0(getDouble("C_R0")), C_R1(getDouble("C_R1")),
  T(Nx_s+1, Ts0),
  T0(Nx_s+1, Ts0),
  W(Nx_s+1, 0),
  alpha(Nx_s+1, 0),
  x(Nx_s, 0),
  rho(Nx_s+1, rho_s), C(Nx_s+1, calcCs(Ts0)), lam(Nx_s+1, lam_s), lam_w(Nx_s+1, lam_s), lam_e(Nx_s+1, lam_s),
  a(Nx_s+1, 0), b(Nx_s+1, 0), c(Nx_s+1, 0), d(Nx_s+1, 0),
  Tw(Ts0)
{

}

std::pair<double, double> SolidPhase1D::solve(double q_g)
{
  setMesh();

  int it = 0;
  int it_dt = 0;
  for (;;)
  {
    it++;
    it_dt++;

    updateThermo();
    
    // Save data at prev step
    m_dot0 = m_dot;

    for (int i = 0; i <= Nx_s; ++i)
    {
      T0[i] = T[i];
    }

    for (int i = 0; i <= Nx_s; ++i)
    {
      W[i] = calcWs(alpha[i], T[i]);
    }
    
    calcMdot();

    // Calc reaction progress equation
    setCoeffsAlpha();
    setLeftBCFixedValue(0);
    setRightBCGradient(0);
    double alpha_max = 0.99; // 1.0;
    double alpha_min = 0;
    TDMA(alpha, relax_alpha, alpha_max, alpha_min);

    // Calc solid energy equation
    setCoeffsT();
    setLeftBCFixedValue(Ts0);
    setRightBCGradient(q_g/lam_w[Nx_s]*dx);
    double T_max = getDouble("Tsmax");
    double T_min = Ts0;
    TDMA(T, relax_T, T_max, T_min);

    checkDt(it_dt);
    if (checkConvergence())
      {
        std::cout << "solid solution converged \n";
        break;
      }
    if (it > N_it) { break; }

    // std::cout << it << " " << m_dot << " " << T[Nx_s] << " " << dt << "\n";
  }
  
  return std::make_pair(m_dot, T[Nx_s]);
}

double SolidPhase1D::calcLams(double Tv) const
{
  const double TvC = Tv - 273.15;
  return (TvC < TmC) ? lam_s : lam_melt;
}

double SolidPhase1D::calcCs(double Tv) const
{
  // return C_s;
  const double TvC = Tv - 273.15;
  return 1000 * ((TvC < TmC) ? C_L0+C_L1*TvC : C_R0+C_R1*TvC);
}

void SolidPhase1D::setMesh()
{
  for (int i = 0; i <= Nx_s; ++i)
  {
    x[i] = i*dx;
  }
}

double SolidPhase1D::calcWs(double alphav, double Tv)
{
  return k_s * pow(1.0 - alphav, ordr_s) * (pow(alphav, ordr_m) + alpha_star) * exp(-E_s / (R0 * Tv));
}

void SolidPhase1D::calcMdot()
{
  // Gauss-Legendre 3-points integration
  const double ksi_1 = 0.1127015; // (-sqrt(3.0 / 5) + 1) / 2;
  const double ksi_2 = 0.5;
  const double ksi_3 = 0.8872983; // ( sqrt(3.0 / 5) + 1) / 2;
  const double h_1 = 0.2777778; // 5.0 / 18;
  const double h_2 = 0.4444444; // 8.0 / 18;
  const double h_3 = 0.2777778; // 5.0 / 18;

  double u_s = 0;

  for (int i = 1; i <= Nx_s; ++i)
  {
    double T_1 = (1.0 - ksi_1) * T[i-1] + ksi_1 * T[i];
    double T_2 = (1.0 - ksi_2) * T[i-1] + ksi_2 * T[i];
    double T_3 = (1.0 - ksi_3) * T[i-1] + ksi_3 * T[i];
    double alpha_1 = (1.0 - ksi_1) * alpha[i-1] + ksi_1 * alpha[i];
    double alpha_2 = (1.0 - ksi_2) * alpha[i-1] + ksi_2 * alpha[i];
    double alpha_3 = (1.0 - ksi_3) * alpha[i-1] + ksi_3 * alpha[i];
    double W_1 = calcWs(alpha_1, T_1);
    double W_2 = calcWs(alpha_2, T_2);
    double W_3 = calcWs(alpha_3, T_3);
    u_s += dx * (h_1*W_1 + h_2*W_2 + h_3*W_3);
  }

  m_dot = rho_s * u_s;
}

void SolidPhase1D::updateThermo()
{
  // Update thermophysics
  for (int i = 0; i <= Nx_s; ++i)
  {
    rho[i] = rho_s;
    C[i] = calcCs(T[i]);
    lam[i] = calcLams(T[i]);
  }

  for (int i = 0; i < Nx_s; ++i)
  {
    lam_e[i] = 2.0*lam[i]*lam[i+1] / (lam[i] + lam[i+1]);
    lam_w[i+1] = lam_e[i];
  }

  Tw = T[Nx_s];
}

void SolidPhase1D::setCoeffsAlpha()
{
  for (int i = 1; i <= Nx_s; ++i)
  {
    double b0 = 1.0/dt;
    a[i] = m_dot/(rho_s*dx);
    c[i] = 0;
    b[i] = a[i] + c[i] + b0;
    d[i] = W[i] + b0*alpha[i];
  }
}

void SolidPhase1D::setCoeffsT()
{
  for (int i = 1; i < Nx_s; ++i)
  {
    double b0 = rho[i]*C[i]/dt;
    a[i] = lam_w[i]/sqr(dx) + C[i]*m_dot/dx;
    c[i] = lam_e[i]/sqr(dx);
    b[i] = a[i] + c[i] + b0;
    d[i] = rho[i]*W[i]*Q_s + b0*T0[i];
  }
}

void SolidPhase1D::setLeftBCFixedValue(double val)
{
  a[0] = 0;
  b[0] = 1.0;
  c[0] = 0;
  d[0] = val;
}

void SolidPhase1D::setRightBCFixedValue(double val)
{
  a[Nx_s] = 0;
  b[Nx_s] = 1.0;
  c[Nx_s] = 0;
  d[Nx_s] = val;
}

void SolidPhase1D::setLeftBCGradient(double grad)
{
  a[0] = 0;
  b[0] = 1.0;
  c[0] = 1.0;
  d[0] = grad;
}

void SolidPhase1D::setRightBCGradient(double grad)
{
  a[Nx_s] = 1.0;
  b[Nx_s] = 1.0;
  c[Nx_s] = 0;
  d[Nx_s] = grad;
}

void SolidPhase1D::relaxCoeffs(std::vector<double> &var, double relax)
{
  for (int i = 1; i < Nx_s; ++i)
  {
    d[i] += (1.0 - relax)/relax * b[i] * var[i];
    b[i] /= relax;
  }
}

void SolidPhase1D::TDMA(std::vector<double> &var, double relax, double var_max, double var_min)
{
  std::vector<double> P(Nx_s+1, 0);
  std::vector<double> Q(Nx_s+1, 0);
  
  P[0] = -c[0]/b[0];
  Q[0] =  d[0]/b[0];

  for (int i = 1; i <= Nx_s; ++i)
    {
      P[i] = -c[i]                / (b[i] + a[i]*P[i-1]);
      Q[i] = (d[i] + a[i]*Q[i-1]) / (b[i] + a[i]*P[i-1]);
    }

  {
    int i = Nx_s;
    double var_new = Q[i];
    if (var_new > var_max) { var_new = var_max; }
    if (var_new < var_min) { var_new = var_min; }
    var[i] = var[i] + relax*(var_new - var[i]);
  }
  
  for (int i = Nx_s - 1; i >= 0; --i)
    {
      double var_new = Q[i] - P[i]*var[i+1];
      if (var_new > var_max) { var_new = var_max; }
      if (var_new < var_min) { var_new = var_min; }
      var[i] = var[i] + relax*(var_new - var[i]);
    }
}

double SolidPhase1D::sqr(double a) const
{
  return a*a;
}

void SolidPhase1D::saveSolution(const std::string fname)
{
  std::ofstream data_out(fname);
  
  data_out << "x_s T_s alpha_s W_s \n";
  for (int i = 0; i <= Nx_s; ++i)
  {
    data_out << x[i] << " " << T[i] << " " << alpha[i] << " " << W[i] << " " << lam[i] << "\n";
  }
  data_out.close();
}

void SolidPhase1D::restoreSolution(const std::string fname)
{
  std::cout << "Restoring the solution " << fname << "\n";
  std::ifstream data_in(fname);
  if (data_in.fail()) { return; }
  
  std::string line;
  std::stringstream ss;
        
  getline(data_in, line);
  
  for (int i = 0; i <= Nx_s; ++i)
  {
    getline(data_in, line);
    ss << line;
    ss >> x[i];
    ss >> T[i];
    ss >> alpha[i];
    ss >> W[i];
    ss.str( std::string() );
    ss.clear();
  }

  data_in.close();
}

bool SolidPhase1D::checkConvergence()
{
  const double m_dot_abs = fabs(m_dot - m_dot0);
  const double m_dot_rel = m_dot_abs/m_dot;
  double dT_abs = 0;
  double dT_rel = 0;

  for (int i = 0; i <= Nx_s; ++i)
  {
    dT_abs += pow(T[i] - T0[i], 2);
    dT_rel += pow((T[i] - T0[i])/T[i], 2);
  }

  dT_abs /= Nx_s;
  dT_rel /= Nx_s;
  
  return ((m_dot_abs < tol_abs) && (m_dot_rel < tol_rel) &&
          (dT_abs < tol_abs) && (dT_rel < tol_rel));
}

void SolidPhase1D::checkDt(int &it_dt)
{
  const double m_dot_abs = fabs(m_dot - m_dot0);
  const double m_dot_rel = m_dot_abs/m_dot;

  if ((it_dt > 100) && (m_dot_rel > 0.3))
    {
      dt *= 0.1;
      // N_it *= 2;
      // tol_abs *= 0.4;
      // tol_rel *= 0.4;
      it_dt = 0;
    }

  if ((it_dt > 1000) && (m_dot_rel < 1e-5))
    {
      dt *= 2;
      it_dt = 0;
    }
}

void SolidPhase1D::reset()
{
  for (int i = 0; i <= Nx_s; ++i)
  {
    T[i] = Ts0;
    T0[i] = Ts0;
    alpha[i] = 0;
  }
}
