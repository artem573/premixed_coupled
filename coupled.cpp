#include <iostream>
#include "solidPhase1D.h"
#include "gasPhase1D.h"
#include "reader.h"


int main()
{
  Reader data("input_data");
  const double tol_abs = data.getDouble("tol_abs");
  const double tol_rel = data.getDouble("tol_rel");
  const int N_it = data.getInt("N_it");
  const double relax = data.getDouble("relax");
  
  const double T_extinct = data.getDouble("T_extinct");
  
  SolidPhase1D sld;
  GasPhase1D gas;

  const std::string initial_g = data.getString("initial_g");
  const std::string initial_s = data.getString("initial_s");
  gas.restoreSolution(initial_g);
  sld.restoreSolution(initial_s);

  double qw0 = data.getDouble("qw0");
  double qw = qw0;
  double m_dot_f0 = data.getDouble("m_dot_f0");
  double Tw_f0 = data.getDouble("T_f0");

  std::ofstream data_out("solution_coupled.dat"); 
  data_out << "it qw_mol qw_rad Tw m_dot \n";

  for (int it = 0; it < N_it; ++it)
  {
    std::cout << "it: " << it << ", solving sld ... ";
    auto [m_dot_f, Tw_f] = sld.solve(qw);
    std::cout << "done";
    m_dot_f = m_dot_f0 + relax*(m_dot_f - m_dot_f0);
    Tw_f = Tw_f0 + relax*(Tw_f - Tw_f0);

    std::cout << ", solving gas ... ";
    auto [qw_mol, qw_rad] = gas.solve(Tw_f, m_dot_f);
    std::cout << "done" << "\n"; 
    qw = qw0 + relax*(qw_mol + qw_rad - qw0);
    
    if (Tw_f < T_extinct)
    {
      std::cout << "flame extinct \n";
      break;
    }

    const double m_dot_abs = fabs(m_dot_f - m_dot_f0);
    const double m_dot_rel = m_dot_abs/m_dot_f;
    const double Tw_abs = fabs(Tw_f - Tw_f0);
    const double Tw_rel = Tw_abs/Tw_f;
  
    if ((m_dot_abs < tol_abs) && (m_dot_rel < tol_rel) &&
        (Tw_abs < tol_abs) && (Tw_rel < tol_rel))
    {
      std::cout << "solution converged \n";
      break;
    }

    data_out << it << " " << qw_mol // << " (" << qw0 << ")"
             << " " << qw_rad
             << " " << Tw_f //<< " (" << Tw_f0 << ")"
             << " " << m_dot_f << "\n"; // << " (" << m_dot_f0 << ")" << "\n";
    data_out.flush();

    m_dot_f0 = m_dot_f;
    Tw_f0 = Tw_f;
    qw0 = qw;

    gas.saveSolutionXML("solution_gas.xml");
    gas.saveSolutionCSV("solution_gas.csv");
    sld.saveSolution("solution_solid.dat");
  }

  // gas.saveSolutionXML("solution_gas.xml");
  // gas.saveSolutionCSV("solution_gas.csv");
  // sld.saveSolution("solution_solid.dat");

  data_out.close();

  return 0;
}
