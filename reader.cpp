#include "reader.h"

Reader::Reader(std::string file_name)
{
  readConsts(file_name);
}

void Reader::readConsts(std::string file_name)
{
  std::string line;
  std::stringstream ss;

  std::ifstream datafile;
  datafile.open(file_name, std::ios::in);

  if (!datafile.is_open())
  {
    std::cout << "Error opening file '" << file_name << "' \n";
    exit(1);
  }

  while (getline(datafile,line))
  {
    ss << line;
    std::string var, val;
    ss >> var;
    if(var != "" and var != "//")
    {
      ss >> val;
      um[var] = val;
    }

    ss.str( std::string() );
    ss.clear();
  }

  datafile.close();
}

int Reader::getInt(std::string var_name) const
{
  std::string::size_type sz;

  std::string var_value = getValue(var_name);
  return std::stoi(um.at(var_name), &sz);
}

double Reader::getDouble(std::string var_name) const
{
  std::string::size_type sz;
  std::string var_value = getValue(var_name);
  return std::stod(um.at(var_name), &sz);
}

std::string Reader::getString(std::string var_name) const
{
  return getValue(var_name);
}

std::string Reader::getValue(std::string var_name) const
{
  std::string tmp;

  try
  {
    tmp = um.at(var_name);
  }
  catch (const std::out_of_range& oor)
  {
    std::cout << "There is no variable named '" << var_name << "' in the read data file \n";
    exit(1);
  }

  return tmp;
}
