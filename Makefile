include /home/artem/Cantera/cantera/build/platform/Cantera.mak

CC=g++
CFLAGS=-std=c++17
CPPFLAGS=$(CANTERA_INCLUDES)
LDFLAGS=
LDLIBS=$(CANTERA_LIBS)
SOURCES=coupled.cpp solidPhase1D.cpp gasPhase1D.cpp reader.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=coupled

all: $(SOURCES)
	$(CC) $(SOURCES) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(LDLIBS) -o $(EXECUTABLE)

fast: $(SOURCES)
	$(CC) $(SOURCES) $(CFLAGS) -O3 -flto $(CPPFLAGS) $(LDFLAGS) $(LDLIBS) -o $(EXECUTABLE)
