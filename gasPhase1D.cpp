#include "gasPhase1D.h"


GasPhase1D::GasPhase1D():
  Reader("input_data"),
  gas_mix(getString("mechanism")),
  flow(&gas_mix),
  trmix(Cantera::newTransportMgr("Mix", &gas_mix)),
  trmulti(newTransportMgr("Multi", &gas_mix)),
  p0(getDouble("p0")),
  width(getDouble("Lx_g")),
  burner_flame(getInt("burner_flame")),
  make_init_guess(true)
{
  if (burner_flame) { flow.setAxisymmetricFlow(); }
  else              { flow.setFreeFlow(); }
    
  setupMesh();

  if (getString("transport") == "Mix") { flow.setTransport(*trmix); }
  else if (getString("transport") == "Multi") { flow.setTransport(*trmulti); }
  else
    {
      throw Cantera::CanteraError("gasPhase1D::gasPhase1D",
                                  "'transport' model shoud be either 'Mix' or 'Multi'");
    }
  
  flow.setKinetics(gas_mix);
  flow.setPressure(p0);

  std::vector<Cantera::Domain1D*> domains { &inlet, &flow, &outlet };
  flame = new Cantera::Sim1D(domains);
}


std::pair<double, double> GasPhase1D::solve(double T_f, double m_dot_f)
{
  double qw_mol = 0;
  double qw_rad = 0;
  
  try
  {
    double m_dot;
    
    // TODO: make getBool with values check
    if (getInt("set_mix_g"))
      {
        // m_dot = m_dot_f;
        m_dot = getDouble("m_dot_mix");

        if (getString("mix_comp_type") == "x")
          {
            gas_mix.setState_TPX(getDouble("T_mix"), p0, getString("mix_comp"));
          }
        else if (getString("mix_comp_type") == "y")
          {
            gas_mix.setState_TPY(getDouble("T_mix"), p0, getString("mix_comp"));
          }
        else
          {
            throw Cantera::CanteraError("gasPhase1D::solve",
                                        "'mix_comp_type' in 'input_data' should be 'x' or 'y'");
          }
      }
    else
      {
        m_dot = mixFandOStreams(gas_mix, T_f, m_dot_f);
      }

    // TODO: read 'F' and 'O' species from 'input_data'
    double phi = gas_mix.massFraction(getString("f_comp")) / gas_mix.massFraction("O2") / (1.0 / getDouble("o2_stoich"));
    std::cout << "phi: " << phi << "\n";
    
    // equiv_ratio = (m_dot_o/m_dot_f) / yo_yf_st;
    double T_in = gas_mix.temperature();

    size_t n_sp = gas_mix.nSpecies();
    Cantera::vector_fp x_in(n_sp);
    gas_mix.getMoleFractions(&x_in[0]);

    inlet.setMoleFractions(x_in.data());
    inlet.setMdot(m_dot);
    inlet.setTemperature(T_in);

    if (make_init_guess)
    {
      makeInitialGuess(m_dot);
      make_init_guess = false;

      // TODO: check if needed
      inlet.setMoleFractions(x_in.data());
      inlet.setMdot(m_dot);
      inlet.setTemperature(T_in);
    }

    if (getInt("solve_energy")) { flow.solveEnergyEqn(); }
    if (getInt("set_fixed_T"))
      {
        Cantera::vector_fp zfixed, tfixed;

        std::string line;
        std::stringstream ss;

        std::ifstream datafile;
        datafile.open("Tprofile", std::ios::in);
        if (!datafile.is_open()) { std::cout << "Error opening file: " << "Tprofile" << "\n"; }

        getline(datafile, line);
        ss << line;
        ss >> line;
        ss.str( std::string() );
        ss.clear();
        while (getline(datafile, line))
          {
            double zp;
            double tp;
            ss << line;
            ss >> zp;
            ss >> tp;
            ss.str( std::string() );
            ss.clear();
            zfixed.push_back(zp/width);
            tfixed.push_back(tp);
          }

        flow.setFixedTempProfile(zfixed, tfixed);
      }
    
    if (getInt("enable_radiation"))
      {
        flow.enableRadiation(true);
        flow.setRadMod(getString("rad_model"));
        flow.setBoundaryEmissivities(getDouble("eps_left"), getDouble("eps_right"));
        flow.setTw(T_f);
      }
    if (getInt("enable_Soret")) { flow.enableSoret(true); }
    if (getInt("enable_soot")) { flow.enableSoot(true); }
    if (getInt("enable_heat_loss_to_env"))
      {
        flow.enableHeatLossEnv(true);
        flow.setDiam(getDouble("diam"));
        flow.setHTC(getDouble("htc"));
      }
    
    int loglevel = 0;
    bool refine_grid = true;

    flame->setRefineCriteria(-1, getDouble("grid_g_ratio"), getDouble("grid_g_slope"), getDouble("grid_g_curve"), getDouble("grid_g_prune"));

    if (getInt("refine_g_x2")) { refineGridX2(); }
    
    if (getString("insert_points") != "none")
      {
        std::vector<double> new_points;
        std::istringstream f(getString("insert_points"));
        std::string s;
        std::string::size_type sz;
        while (getline(f, s, ','))
          {
            new_points.push_back(std::stod(s, &sz));
          }

        for (const auto & point : new_points)
          {
            flame->insertGridPoint(flow.domainIndex(), point);
          }
      }

    if (getInt("set_Tfix")) { setFixedT(); }
    if (getInt("set_qext")) { setExtQ(); }
    
    
    // flame->solve(loglevel, false);
    flame->refine();
    flame->solve(loglevel, refine_grid);
    
    size_t ind0 = 0;
    size_t ind1 = 1;

    doublereal lam0 = flow.thermalConductivity(ind0);
    // doublereal lam1 = flow.thermalConductivity(ind1);
    doublereal T0 = flame->value(flow.domainIndex(), flow.componentIndex("T"), ind0);
    doublereal T1 = flame->value(flow.domainIndex(), flow.componentIndex("T"), ind1);

    // qw_mol > 0 and qw_rad > 0 - flux from flame to solid
    qw_mol = lam0 * (T1 - T0) / (flow.grid()[ind1] - flow.grid()[ind0]);
    if (getInt("enable_radiation")) { qw_rad = -flow.radFluxLeft(); }

    // for (size_t j = 0; j < flow.nPoints(); ++j)
    // {
    //   std::cout << flow.grid()[j] << " " << flow.radHeatLoss(j) << " " << flame->value(flow.domainIndex(), flow.componentIndex("T"), j) << "\n";
    // }
  }
  catch (Cantera::CanteraError& err)
  {
    std::cerr << err.what() << std::endl;
    std::cerr << "program terminating." << std::endl;
    return std::make_pair(-1, -1);
  }

  return std::make_pair(qw_mol, qw_rad);
}

void GasPhase1D::setupMesh()
{
  // Create an initial grid
  int nz = 6; // 41;
  // doublereal lz = width;

  Cantera::vector_fp z(nz);

  z[0] = 0;
  z[1] = 1e-5;
  z[2] = 0.4*width;
  z[3] = 0.48*width;
  z[4] = 0.6*width;
  z[5] = width;
  
  flow.setupGrid(nz, &z[0]);
}

void GasPhase1D::makeInitialGuess(double m_dot)
{
  size_t n_sp = gas_mix.nSpecies();

  Cantera::vector_fp y_in(n_sp);
  gas_mix.getMassFractions(&y_in[0]);
  double rho_in = gas_mix.density();
  double u_in = m_dot / rho_in;

  double T0 = gas_mix.temperature();
  
  gas_mix.equilibrate("HP");
  Cantera::vector_fp y_out(n_sp);
  gas_mix.getMassFractions(&y_out[0]);
  doublereal rho_out = gas_mix.density();
  doublereal u_out = m_dot / rho_out;

  doublereal T_ad = gas_mix.temperature();
  doublereal T1 = T_ad;
  
  Cantera::vector_fp locs {0.0, 1e-10, 0.4, 0.6, 1.0};
  // Cantera::vector_fp locs {0.0, 0.4, 0.6, 1.0};
  Cantera::vector_fp value;

  if (burner_flame) { value = {u_in, u_out, u_out, u_out, u_out}; }
  else              { value = {u_in, u_in, u_in, u_out, u_out}; }
  flame->setInitialGuess("u", locs, value);
  if (burner_flame) { value = {T0, T1, T1, T1, T1}; }
  else              { value = {T0, T0, T0, T1, T1}; }
  flame->setInitialGuess("T", locs, value);

  // TODO: move 'flame->setFixedTemperature' inside 'gasPhase1D::solve' method
  if (!burner_flame) { flame->setFixedTemperature(0.5 * (T0 + T_ad)); }

  for (size_t i = 0; i < n_sp; i++)
  {
    if (burner_flame) { value = {y_in[i], y_out[i], y_out[i], y_out[i], y_out[i]}; }
    else              { value = {y_in[i], y_in[i], y_in[i], y_out[i], y_out[i]}; }
    flame->setInitialGuess(gas_mix.speciesName(i), locs, value);
  }
}

void GasPhase1D::saveSolutionXML(const std::string fname)
{
  flame->save(fname, "id1", "test_description");
}

void GasPhase1D::saveSolutionCSV(const std::string fname)
{
  std::ofstream outfile_x("x"+fname);
  std::ofstream outfile_y("y"+fname);

  Cantera::IdealGasMix gas(getString("mechanism"));
  size_t n_sp = gas.nSpecies();

  size_t n_comp = 0;
  std::string end_key = "<unknown>";
  // !!! TODO: rewrite extort in csv, get n_comp from Cantera
  size_t index = 0;
  while (1)
    {
      if (flow.componentName(index) == end_key)
        {
          n_comp = index-1;
          break;
        }
      index++;
    }
  
  outfile_x << "z(m)";
  outfile_y << "z(m)";
  for (size_t j = 0; j <= n_comp; ++j)
  {
    outfile_x << "," << flow.componentName(j);
    outfile_y << "," << flow.componentName(j);
  }
  outfile_x << "\n";
  outfile_y << "\n";
  
  for (size_t j = 0; j < flow.nPoints(); ++j)
  {
    outfile_x << flow.grid(j);
    outfile_y << flow.grid(j);

    for (size_t comp = 0; comp <= n_comp-n_sp; ++comp)
      {
        outfile_x << "," << flame->value(flow.domainIndex(), comp, j);
        outfile_y << "," << flame->value(flow.domainIndex(), comp, j);
      }        
    
    Cantera::vector_fp y_gas(n_sp);

    for (size_t sp_i = 0; sp_i < n_sp; ++sp_i)
      {
        const std::string sp_name = gas.speciesName(sp_i);
        y_gas[sp_i] = flame->value(flow.domainIndex(), flow.componentIndex(sp_name), j);
      }

    gas.setMassFractions(&y_gas[0]);
    Cantera::vector_fp x_gas(n_sp);
    gas.getMoleFractions(&x_gas[0]);

    for (size_t comp_i = n_comp-n_sp+1; comp_i <= n_comp; ++comp_i)
      {
        const std::string comp_name = flow.componentName(comp_i);
        const size_t sp_i = gas.speciesIndex(comp_name);
        
        outfile_y << "," << y_gas[sp_i];
        outfile_x << "," << x_gas[sp_i];
      }

    outfile_x << "\n";
    outfile_y << "\n";
  }

  outfile_x.close();
  outfile_y.close();
}

void GasPhase1D::restoreSolution(const std::string fname)
{
  if (fname == "none") { return; }
  std::cout << "Restoring the solution " << fname << "\n";
  flame->restore(fname, "id1");
  make_init_guess = false;
}

double GasPhase1D::mixFandOStreams(Cantera::IdealGasMix& gas_mix, double T_f, double m_dot_f)
{
  const double m_dot_o(getDouble("m_dot_o"));
  const double T_o(getDouble("T_o"));

  Cantera::IdealGasMix gas_f(getString("mechanism"));
  Cantera::IdealGasMix gas_o(getString("mechanism"));
  
  gas_o.setState_TPY(T_o, p0, getString("o_comp_y"));
  gas_f.setState_TPY(T_f, p0, getString("f_comp_y"));

  double cp_f = gas_f.cp_mass();
  double cp_o = gas_o.cp_mass();
  double m_dot_mix = m_dot_f + m_dot_o;

  size_t n_sp = gas_mix.nSpecies();

  Cantera::vector_fp y_f(n_sp);
  gas_f.getMassFractions(&y_f[0]);
  Cantera::vector_fp y_o(n_sp);
  gas_o.getMassFractions(&y_o[0]);
  Cantera::vector_fp y_mix(n_sp, 0);
  for (size_t i = 0; i < n_sp; ++i)
  {
    y_mix[i] = (y_f[i]*m_dot_f + y_o[i]*m_dot_o) / m_dot_mix;
  }

  double cp_mix = (cp_f*m_dot_f + cp_o*m_dot_o) / m_dot_mix;
  double T_mix = (cp_f*T_f*m_dot_f + cp_o*T_o*m_dot_o) / (m_dot_mix * cp_mix);
  gas_mix.setState_TPY(T_mix, p0, &y_mix[0]);

  return m_dot_mix;
}

void GasPhase1D::setFixedT()
{
  const double x_Tfix = getDouble("x_Tfix");
  const double Tfix = getDouble("Tfix");
  
  flame->insertGridPoint(flow.domainIndex(), x_Tfix);
  flow.setTemperatureCustom(x_Tfix, Tfix);
  flame->setValue(flow.domainIndex(), flow.componentIndex("T"), flow.getGridIndex(x_Tfix), Tfix);
}

void GasPhase1D::setExtQ()
{
  const double x_qext_start = getDouble("x_qext_start");
  const double x_qext_l = getDouble("x_qext_l");
  const double qext = getDouble("qext");
  
  flame->insertGridPoint(flow.domainIndex(), x_qext_start);
  flame->insertGridPoint(flow.domainIndex(), x_qext_start + 0.5*x_qext_l);
  flame->insertGridPoint(flow.domainIndex(), x_qext_start + x_qext_l);
  flow.setqext(x_qext_start, x_qext_l, qext);
}

void GasPhase1D::refineGridX2()
{
  std::vector<double> new_points;
  for (size_t i = 0; i < flow.nPoints()-1; ++i)
    {
      new_points.push_back(0.5*(flow.grid()[i] + flow.grid()[i+1]));
    }

  for (const auto & point: new_points)
    {
      flame->insertGridPoint(flow.domainIndex(), point);
    }
}
